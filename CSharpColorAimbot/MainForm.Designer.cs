﻿namespace CSharpColorAimbot
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lbInfoSelProc = new System.Windows.Forms.Label();
            this.lbSelectedProcess = new System.Windows.Forms.Label();
            this.gbProc = new System.Windows.Forms.GroupBox();
            this.btnSelProc = new System.Windows.Forms.Button();
            this.gbFeatures = new System.Windows.Forms.GroupBox();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnConfigureAimbot = new System.Windows.Forms.Button();
            this.btnClearConsole = new System.Windows.Forms.Button();
            this.btnDisableAimbot = new System.Windows.Forms.Button();
            this.btnEnableAimbot = new System.Windows.Forms.Button();
            this.gbLogging = new System.Windows.Forms.GroupBox();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.gbProc.SuspendLayout();
            this.gbFeatures.SuspendLayout();
            this.gbLogging.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbInfoSelProc
            // 
            this.lbInfoSelProc.AutoSize = true;
            this.lbInfoSelProc.Location = new System.Drawing.Point(133, 30);
            this.lbInfoSelProc.Name = "lbInfoSelProc";
            this.lbInfoSelProc.Size = new System.Drawing.Size(93, 13);
            this.lbInfoSelProc.TabIndex = 1;
            this.lbInfoSelProc.Text = "Selected Process:";
            // 
            // lbSelectedProcess
            // 
            this.lbSelectedProcess.AutoSize = true;
            this.lbSelectedProcess.Location = new System.Drawing.Point(232, 30);
            this.lbSelectedProcess.Name = "lbSelectedProcess";
            this.lbSelectedProcess.Size = new System.Drawing.Size(0, 13);
            this.lbSelectedProcess.TabIndex = 2;
            // 
            // gbProc
            // 
            this.gbProc.Controls.Add(this.btnSelProc);
            this.gbProc.Controls.Add(this.lbSelectedProcess);
            this.gbProc.Controls.Add(this.lbInfoSelProc);
            this.gbProc.Location = new System.Drawing.Point(7, 7);
            this.gbProc.Name = "gbProc";
            this.gbProc.Size = new System.Drawing.Size(405, 61);
            this.gbProc.TabIndex = 3;
            this.gbProc.TabStop = false;
            this.gbProc.Text = "Process Information";
            // 
            // btnSelProc
            // 
            this.btnSelProc.Image = ((System.Drawing.Image)(resources.GetObject("btnSelProc.Image")));
            this.btnSelProc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelProc.Location = new System.Drawing.Point(5, 17);
            this.btnSelProc.Name = "btnSelProc";
            this.btnSelProc.Size = new System.Drawing.Size(122, 39);
            this.btnSelProc.TabIndex = 0;
            this.btnSelProc.Text = "Select Process";
            this.btnSelProc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelProc.UseVisualStyleBackColor = true;
            this.btnSelProc.Click += new System.EventHandler(this.btnSelProc_Click);
            // 
            // gbFeatures
            // 
            this.gbFeatures.Controls.Add(this.btnAbout);
            this.gbFeatures.Controls.Add(this.btnConfigureAimbot);
            this.gbFeatures.Controls.Add(this.btnClearConsole);
            this.gbFeatures.Controls.Add(this.btnDisableAimbot);
            this.gbFeatures.Controls.Add(this.btnEnableAimbot);
            this.gbFeatures.Location = new System.Drawing.Point(7, 74);
            this.gbFeatures.Name = "gbFeatures";
            this.gbFeatures.Size = new System.Drawing.Size(405, 93);
            this.gbFeatures.TabIndex = 4;
            this.gbFeatures.TabStop = false;
            this.gbFeatures.Text = "Aimbot configuration";
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(324, 67);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 4;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnConfigureAimbot
            // 
            this.btnConfigureAimbot.Image = global::CSharpColorAimbot.Properties.Resources.ConfigureAimbot;
            this.btnConfigureAimbot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfigureAimbot.Location = new System.Drawing.Point(6, 19);
            this.btnConfigureAimbot.Name = "btnConfigureAimbot";
            this.btnConfigureAimbot.Size = new System.Drawing.Size(134, 42);
            this.btnConfigureAimbot.TabIndex = 3;
            this.btnConfigureAimbot.Text = "Configure Aimbot";
            this.btnConfigureAimbot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfigureAimbot.UseVisualStyleBackColor = true;
            this.btnConfigureAimbot.Click += new System.EventHandler(this.btnConfigureTriggerColor_Click);
            // 
            // btnClearConsole
            // 
            this.btnClearConsole.Location = new System.Drawing.Point(233, 67);
            this.btnClearConsole.Name = "btnClearConsole";
            this.btnClearConsole.Size = new System.Drawing.Size(85, 23);
            this.btnClearConsole.TabIndex = 2;
            this.btnClearConsole.Text = "Clear Console";
            this.btnClearConsole.UseVisualStyleBackColor = true;
            this.btnClearConsole.Click += new System.EventHandler(this.btnClearConsole_Click);
            // 
            // btnDisableAimbot
            // 
            this.btnDisableAimbot.Enabled = false;
            this.btnDisableAimbot.Image = global::CSharpColorAimbot.Properties.Resources.DisableAimbot;
            this.btnDisableAimbot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisableAimbot.Location = new System.Drawing.Point(271, 19);
            this.btnDisableAimbot.Name = "btnDisableAimbot";
            this.btnDisableAimbot.Size = new System.Drawing.Size(128, 42);
            this.btnDisableAimbot.TabIndex = 1;
            this.btnDisableAimbot.Text = "Disable Aimbot";
            this.btnDisableAimbot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisableAimbot.UseVisualStyleBackColor = true;
            this.btnDisableAimbot.Click += new System.EventHandler(this.btnDisableAimbot_Click);
            // 
            // btnEnableAimbot
            // 
            this.btnEnableAimbot.Image = global::CSharpColorAimbot.Properties.Resources.EnableAimbot;
            this.btnEnableAimbot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnableAimbot.Location = new System.Drawing.Point(146, 19);
            this.btnEnableAimbot.Name = "btnEnableAimbot";
            this.btnEnableAimbot.Size = new System.Drawing.Size(119, 42);
            this.btnEnableAimbot.TabIndex = 0;
            this.btnEnableAimbot.Text = "Enable Aimbot";
            this.btnEnableAimbot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEnableAimbot.UseVisualStyleBackColor = true;
            this.btnEnableAimbot.Click += new System.EventHandler(this.btnEnableAimbot_Click);
            // 
            // gbLogging
            // 
            this.gbLogging.Controls.Add(this.tbConsole);
            this.gbLogging.Location = new System.Drawing.Point(7, 168);
            this.gbLogging.Name = "gbLogging";
            this.gbLogging.Size = new System.Drawing.Size(405, 158);
            this.gbLogging.TabIndex = 5;
            this.gbLogging.TabStop = false;
            this.gbLogging.Text = "Logging Console";
            // 
            // tbConsole
            // 
            this.tbConsole.Location = new System.Drawing.Point(6, 19);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.ReadOnly = true;
            this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbConsole.Size = new System.Drawing.Size(393, 132);
            this.tbConsole.TabIndex = 0;
            this.tbConsole.TextChanged += new System.EventHandler(this.tbConsole_TextChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 331);
            this.Controls.Add(this.gbLogging);
            this.Controls.Add(this.gbFeatures);
            this.Controls.Add(this.gbProc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Color Aimbot by evolution536";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.gbProc.ResumeLayout(false);
            this.gbProc.PerformLayout();
            this.gbFeatures.ResumeLayout(false);
            this.gbLogging.ResumeLayout(false);
            this.gbLogging.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSelProc;
        private System.Windows.Forms.Label lbInfoSelProc;
        internal System.Windows.Forms.Label lbSelectedProcess;
        private System.Windows.Forms.GroupBox gbProc;
        private System.Windows.Forms.GroupBox gbFeatures;
        private System.Windows.Forms.Button btnEnableAimbot;
        private System.Windows.Forms.GroupBox gbLogging;
        private System.Windows.Forms.TextBox tbConsole;
        private System.Windows.Forms.Button btnDisableAimbot;
        private System.Windows.Forms.Button btnClearConsole;
        private System.Windows.Forms.Button btnConfigureAimbot;
        private System.Windows.Forms.Button btnAbout;
    }
}

