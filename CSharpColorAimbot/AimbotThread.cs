﻿using System;
using System.Drawing;
using System.Threading;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Represents a worker thread that runs as child under the aimbot thread manager.
    /// </summary>
    public sealed class AimbotThread
    {
        bool mIsRunning;

        public AimbotThread()
        {
            
        }

        /// <summary>
        /// Occurs when the thread found a matched pixel and reports to the thread manager.
        /// In case of pixel skipping, this event will be triggered after the pixel skip count is passed.
        /// </summary>
        public event ColorAimbotAimedEventHandler PixelFound;

        /// <summary>
        /// Starts the thread and changes it's state to running.
        /// </summary>
        public void Start(Object threadContext)
        {
            mIsRunning = true;
            AimbotThreadData mThreadData = threadContext as AimbotThreadData;
            AimbotThreadManager tmg = AimbotThreadManager.GetInstance();
            ColorPair triggerColor = tmg.TriggerColor;
            Func<int, int, int, bool> cmpFunc = mThreadData.CompareFunction;

            // Iterate through the pixels by X-axis.
            for (int column = 0; column < mThreadData.Height; ++column)
            {
                int currentColumnStrideIndex = column * mThreadData.Stride;

                // Iterate through the pixels by Y-axis.
                for (int row = 0; row < mThreadData.Width; ++row)
                {
                    int rowComparisonIndex = row * 4;

                    // Compare the RGB values of the current pixel to the by the user configured color.
                    // Statement will be true when the RGB values exactly match the configured color.
                    if (cmpFunc(mThreadData.OriginalBitmapData[currentColumnStrideIndex + rowComparisonIndex + 2]
                        , mThreadData.OriginalBitmapData[currentColumnStrideIndex + rowComparisonIndex + 1]
                        , mThreadData.OriginalBitmapData[currentColumnStrideIndex + rowComparisonIndex]))
                    {
                        // Check wether the still should still be running. If it shouldn't the thread must exit.
                        if (!mIsRunning)
                        {
                            break;
                        }

                        // Pixel Skipping functionality. Skips "mPixelSkipCount" pixels when the value is
                        // greater than zero.
                        if (mThreadData.PixelSkipCount > 0)
                        {
                            // Decrement skip count. When the skipcount reaches zero skipping for the current
                            // screen capture is done.
                            --mThreadData.PixelSkipCount;
                        }
                        else
                        {
                            // Fire the PixelFound event so the manager knows that it should stop the threads.
                            PixelFound(new MouseCoordinate((mThreadData.WindowLeftPosition + row)
                                , (mThreadData.WindowTopPosition + column)));

                            // A pixel is found, this thread's work is done. Exit the loop.
                            mIsRunning = false;
                            break;
                        }
                    }
                }

                // Check also here wether the still should still be running. If it shouldn't the thread must exit.
                if (!mIsRunning)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Stops the thread and changes it's state to stopped.
        /// </summary>
        public void Stop()
        {
            mIsRunning = false;
        }

        /// <summary>
        /// Indicates wether the thread is currently running or not.
        /// </summary>
        public bool IsRunning { get { return mIsRunning; } }
    }
}
