﻿// ===============================================================================================================================
// MainForm.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace CSharpColorAimbot
{
    /// <summary>
    /// The ColorAimbot main window. This window is started as GUI.
    /// </summary>
    public sealed partial class MainForm : Form
    {
        [DllImport("User32.dll")]
        static extern bool LockWindowUpdate(IntPtr hwndLock);

        internal string mGameProc;
        internal AimbotThreadManager clrambt;

        public MainForm()
        {
            InitializeComponent();

            clrambt = AimbotThreadManager.GetInstance();
            clrambt.TriggerColor.FirstColor = Properties.Settings.Default.Triggercolor;
        }

        private void btnSelProc_Click(object sender, EventArgs e)
        {
            SelectProcessForm frm = new SelectProcessForm(this);
            frm.ShowDialog();
        }

        private void btnEnableAimbot_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(mGameProc))
            {
                MessageBox.Show("There is no process selected!", "Input Error", MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
                return;
            }

            if (AimbotThreadManager.GetInstance().TriggerColor.FirstColor.IsEmpty)
            {
                if (MessageBox.Show("You haven't selected a color. This means the aimbot will respond to RGB(0, 0, 0), making the mouse"
                    + " jump to undefined spots on the screen. Are you sure you want to activate the aimbot?", "Are you sure?"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
            }

            btnDisableAimbot.Enabled = true;
            btnEnableAimbot.Enabled = false;
            btnConfigureAimbot.Enabled = false;

            clrambt.AimedAtPosition += clrambt_AimedAtPosition;
            clrambt.AimbotErrorOccured += clrambt_AimbotErrorOccured;
            clrambt.AimbotReports += clrambt_AimbotReports;

            if (Properties.Settings.Default.EnablePixelSkipping)
            {
                clrambt.StartAimbotMultithreaded(mGameProc, Properties.Settings.Default.PixelSkippingNumber);
            }
            else
            {
                clrambt.StartAimbotMultithreaded(mGameProc, 0);
            }

            if (!string.IsNullOrEmpty(tbConsole.Text))
            {
                tbConsole.Text += "------------------------------------------------------------------------------\r\n";
            }
        }

        void clrambt_AimbotReports(string pMessage)
        {
            tbConsole.Invoke(new MethodInvoker(() =>
            {
                tbConsole.Text += pMessage;

                if (!clrambt.IsRunning)
                {
                    btnDisableAimbot.Enabled = false;
                    btnEnableAimbot.Enabled = true;
                    btnConfigureAimbot.Enabled = true;
                }
            }));
        }

        void clrambt_AimbotErrorOccured(string pMessage)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                MessageBox.Show(pMessage, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (!clrambt.IsRunning)
                {
                    btnDisableAimbot.Enabled = false;
                    btnEnableAimbot.Enabled = true;
                    btnConfigureAimbot.Enabled = true;
                }
            }));
        }

        void clrambt_AimedAtPosition(MouseCoordinate pCoord)
        {
            tbConsole.Invoke(new MethodInvoker(() =>
            {
                LockWindowUpdate(tbConsole.Handle);

                tbConsole.Text += DateTime.Now.ToLongTimeString()
                    + " - ColorAimbot.SetCursorPos() at X: " + pCoord.X.ToString() + " and Y: " + pCoord.Y.ToString()
                    + Environment.NewLine;

                tbConsole.SelectionStart = tbConsole.Text.Length;
                tbConsole.ScrollToCaret();

                LockWindowUpdate(IntPtr.Zero);
            }));
        }

        private void btnDisableAimbot_Click(object sender, EventArgs e)
        {
            clrambt.StopAimbot();

            btnDisableAimbot.Enabled = false;
            btnEnableAimbot.Enabled = true;
            btnConfigureAimbot.Enabled = true;

            tbConsole.SelectionStart = tbConsole.Text.Length;
            tbConsole.ScrollToCaret();
        }

        private void btnClearConsole_Click(object sender, EventArgs e)
        {
            tbConsole.Clear();
        }

        private void btnConfigureTriggerColor_Click(object sender, EventArgs e)
        {
            AimbotConfigurationForm acf = new AimbotConfigurationForm();
            acf.ShowDialog();
        }

        private void tbConsole_TextChanged(object sender, EventArgs e)
        {
            // We would want to save a little memory, since by playing games using the aimbot the log can get
            // really big. When the log passes 10000 lines it will be cleared. You can ofcourse turn this off by
            // commenting this code out.
            if (tbConsole.Lines.Length > 10000)
            {
                tbConsole.Clear();
            }
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutForm abtFrm = new AboutForm();
            abtFrm.ShowDialog();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (clrambt.IsRunning)
            {
                clrambt.StopAimbot();
            } 

            btnDisableAimbot.Enabled = false;
            btnEnableAimbot.Enabled = true;
            btnConfigureAimbot.Enabled = true;

            tbConsole.SelectionStart = tbConsole.Text.Length;
            tbConsole.ScrollToCaret();
        }
    }
}
