﻿// ===============================================================================================================================
// Win32.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Runtime.InteropServices;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Represents the Win32 API native type: struct RECT.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }

    /// <summary>
    /// Encapsulates Win32 native API calls.
    /// </summary>
    public static class NativeMethods
    {
        /// <summary>
        /// Gets the bounds of a Win32 window using the window handle.
        /// </summary>
        /// <param name="hWnd">The window handle of the target window.</param>
        /// <param name="rect">The RECT struct used to put the output values in.</param>
        /// <returns>Wether the function succeeded or not.</returns>
        [DllImport("User32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, ref Rect rect);

        /// <summary>
        /// Sets the cursor position on a specified position on screen.
        /// </summary>
        /// <param name="x">The X position to set the cursor to.</param>
        /// <param name="y">The Y position to set the cursor to.</param>
        /// <returns>Wether the function succeeded or not.</returns>
        [DllImport("User32.dll")]
        public static extern bool SetCursorPos(int x, int y);
    }
}