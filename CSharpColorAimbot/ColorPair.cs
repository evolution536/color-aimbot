﻿using System;
using System.Drawing;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Represents a range of color values to match pixels to. This class cannot be inherited.
    /// </summary>
    public sealed class ColorPair
    {
        Color c1;
        Color c2;

        /// <summary>
        /// Constructs a default color pair with (0, 0, 0).
        /// </summary>
        public ColorPair()
        {

        }

        /// <summary>
        /// Constructs a color pair with the specified colors.
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        public ColorPair(Color c1, Color c2)
        {
            this.c1 = c1;
            this.c2 = c2;
        }

        /// <summary>
        /// Gets the left- or first color part of the pair. This is the starting color value of the range.
        /// </summary>
        public Color FirstColor
        {
            get
            {
                return this.c1;
            }
            set
            {
                this.c1 = value;
            }
        }

        /// <summary>
        /// Gets the right- or second color part of the pair. This is the ending color value of the range.
        /// </summary>
        public Color SecondColor
        {
            get
            {
                return this.c2;
            }
            set
            {
                this.c2 = value;
            }
        }

        /// <summary>
        /// Finds out whether a color value is in range of the configured color values. Use this function to match a range of colors.
        /// </summary>
        /// <param name="c">The color value to match.</param>
        /// <returns>Whether the input color value is between the starting and ending color value.</returns>
        public bool IsInRange(int r, int g, int b)
        {
            return ((r >= this.c1.R) && (g >= this.c1.G) && (b >= this.c1.B)) && ((r <= this.c2.R) && (g <= this.c2.G) && (b <= this.c2.B));
        }

        /// <summary>
        /// Finds out whether a color value matches the first color in the color pair. Use this function to match exactly one color.
        /// </summary>
        /// <param name="r">The red color part of the input color.</param>
        /// <param name="g">The green color part of the input color.</param>
        /// <param name="b">The blue color part of the input color.</param>
        /// <returns>Whether the input color value matches the first color in the color pair.</returns>
        public bool IsSingleColorMatch(int r, int g, int b)
        {
            return (this.c1.R == r) && (this.c1.G == b) && (this.c1.B == b);
        }
    }
}