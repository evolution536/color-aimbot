﻿// ===============================================================================================================================
// AimbotThreadManager.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Delegate type used by the aimbot to handle reporting to the log when the aimbot is triggered.
    /// </summary>
    /// <param name="pCoord">The coordinates on the screen that triggered the aimbot.</param>
    public delegate void ColorAimbotAimedEventHandler(MouseCoordinate pCoord);

    /// <summary>
    /// Delegate type used by the aimbot to handle errors.
    /// </summary>
    /// <param name="pMessage"></param>
    public delegate void ColorAimbotErrorEventHandler(string pMessage);

    /// <summary>
    /// Delegate type used by the aimbot to handle reports.
    /// </summary>
    /// <param name="pMessage"></param>
    public delegate void ColorAimbotReportEventHandler(string pMessage);

    /// <summary>
    /// The thread manager for multithread enabled ColorAimbot access.
    /// </summary>
    public sealed class AimbotThreadManager
    {
        static AimbotThreadManager mInstance;

        bool mRunning;
        bool mPixelFound;

        AimbotThread mChildThread = new AimbotThread();

        AimbotThreadManager() { }

        /// <summary>
        /// Gets the instance of the AimbotThreadManager class. It is a singleton.
        /// </summary>
        /// <returns>The only existing instance of AimbotThreadManager.</returns>
        public static AimbotThreadManager GetInstance()
        {
            if (mInstance == null)
            {
                mInstance = new AimbotThreadManager();
                mInstance.TriggerColor = new ColorPair();
            }

            return mInstance;
        }

        /// <summary>
        /// Occurs when the aimbot is triggered. The aimbot gets triggered when a color match appears on the screen.
        /// </summary>
        public event ColorAimbotAimedEventHandler AimedAtPosition;

        /// <summary>
        /// Occurs when an exception is thrown by the aimbot.
        /// </summary>
        public event ColorAimbotErrorEventHandler AimbotErrorOccured;

        /// <summary>
        /// Occurs when the aimbot needs to report to the GUI.
        /// </summary>
        public event ColorAimbotReportEventHandler AimbotReports;

        /// <summary>
        /// Indicates wether the aimbot is running or not.
        /// </summary>
        public bool IsRunning { get { return mRunning; } }

        /// <summary>
        /// The color that is used by the aimbot to match pixels on the screen. When a pixel matches this color the aimbot gets triggered.
        /// A static variable is used because a property slows down the application.
        /// </summary>
        public ColorPair TriggerColor;

        /// <summary>
        /// Starts the aimbot sequence using the specified amount of pixels to skip.
        /// </summary>
        public void StartAimbotMultithreaded(string processName, int pPixelSkipCount)
        {
            ThreadPool.SetMaxThreads(Environment.ProcessorCount, Environment.ProcessorCount);

            // Save the compare mode up on top to avoid multiple property lookups.
            bool compareMode = Properties.Settings.Default.ColorSelectionMode == 1;

            // Start manager thread to ensure GUI responsiveness.
            ThreadPool.QueueUserWorkItem(delegate(object o)
            {
                // Get HWND of target process main window. This is needed to capture the screen to a bitmap.
                IntPtr mProc = Process.GetProcessesByName(processName)[0].MainWindowHandle;

                // The selected process is invalid. Break out of the function.
                if (mProc == IntPtr.Zero)
                {
                    AimbotErrorOccured("Failed to enable aimbot. The selected process does not have a main window.");
                    this.StopAimbot();
                    return;
                }

                mRunning = true;

                // Only report when user required to.
                if (Properties.Settings.Default.LogStartStop)
                {
                    // Report to the GUI that the aimbot has started and changed state running.
                    StringBuilder sb = new StringBuilder(DateTime.Now.ToLongTimeString());
                    sb.Append(" - Target process: ");
                    sb.Append(processName);
                    sb.Append(Environment.NewLine);
                    this.AimbotReports(sb.ToString());
                    sb = new StringBuilder(DateTime.Now.ToLongTimeString());
                    sb.Append(" - ColorAimbot running...");
                    sb.Append(Environment.NewLine);
                    sb = new StringBuilder(DateTime.Now.ToLongTimeString());
                    sb.Append(" - " + (compareMode ? "Color range: " : "Single color: "));
                    sb.Append("(" + this.TriggerColor.FirstColor.R + ", " + this.TriggerColor.FirstColor.G + ", " + this.TriggerColor.FirstColor.B + ")");
                    if (compareMode)
                    {
                        sb.Append(" - (" + this.TriggerColor.SecondColor.R + ", " + this.TriggerColor.SecondColor.G + ", " + this.TriggerColor.SecondColor.B + ")");
                    }
                    sb.Append(Environment.NewLine);
                    this.AimbotReports(sb.ToString());
                }

                // Assign pixel found event to thread delegate.
                this.mChildThread.PixelFound += childThread_PixelFound;

                // Save the compare function only once per run. There is no need to keep saving it each loop iteration.
                Func<int, int, int, bool> compareFunc = compareMode ? (Func<int, int, int, bool>)this.TriggerColor.IsInRange : this.TriggerColor.IsSingleColorMatch;

                while (mRunning)
                {
                    // Reset the termination indicator.
                    this.mPixelFound = false;

                    // Save the bounds of the target window to a RECT struct instance.
                    Rect hRect = new Rect();
                    NativeMethods.GetWindowRect(mProc, ref hRect);

                    // Calculate future capture (target window) width and height.
                    int width = hRect.Right - hRect.Left;
                    int height = hRect.Bottom - hRect.Top;

                    // Failed to save window bounds. The height or width is 0, which should be impossible.
                    // Report error to hthe GUI and break of of the function.
                    if (width == 0 || height == 0)
                    {
                        this.AimbotErrorOccured("Failed to create capture. Is the target process or it's main window closed?");
                        this.StopAimbot();
                        return;
                    }

                    // Capture the target window to a bitmap and store it in a Bitmap variable.
                    Bitmap hBmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                    Graphics hGraphics = Graphics.FromImage(hBmp);
                    hGraphics.CopyFromScreen(hRect.Left, hRect.Top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

                    // Lock the bitwise data of the stored bitmap and create a data structure for us to access it.
                    Rectangle rect = new Rectangle(0, 0, hBmp.Width, hBmp.Height);
                    BitmapData bmpData = hBmp.LockBits(rect, ImageLockMode.ReadOnly, hBmp.PixelFormat);

                    // Split bitmap RGB data into multiple pieces so each child thread can render it's part.
                    AimbotThreadData hCutData = new AimbotThreadData();

                    // Initialize the thread data array sized to the length of the bitmap to store the bitwise data from above.
                    int bytes = (bmpData.Stride * hBmp.Height);
                    hCutData.OriginalBitmapData = new byte[bytes];

                    // Copy the bitwise data into the created array.
                    Marshal.Copy(bmpData.Scan0, hCutData.OriginalBitmapData, 0, bytes);

                    // These parameters are needed for the pixel iterator to run.
                    hCutData.Stride = bmpData.Stride;
                    hCutData.Height = bmpData.Height;
                    hCutData.Width = bmpData.Width;
                    hCutData.PixelSkipCount = pPixelSkipCount;
                    hCutData.WindowLeftPosition = hRect.Left;
                    hCutData.WindowTopPosition = hRect.Top;
                    hCutData.CompareFunction = compareFunc;

                    // Release the lock upon the bitwise bitmap data.
                    hBmp.UnlockBits(bmpData);

                    // Dispose used objects. If you don't do this you will create a sick memory leak.
                    hGraphics.Dispose();
                    hBmp.Dispose();

                    // Add new task to thread pool.
                    ThreadPool.QueueUserWorkItem(this.mChildThread.Start, hCutData);
                }
            });
        }

        void childThread_PixelFound(MouseCoordinate pCoord)
        {
            // If there is no pixel found yet, move the cursor. If there already is, there is no need to do so.
            if (!mPixelFound)
            {
                this.MoveCursor(pCoord.X, pCoord.Y);
                this.SendInterrupt();
            }

            // Set pixel found to true to indicate that a pixel is found and in case a thread finds one before termination
            // it's report does not trigger the cursor movement. This would cause inconsisten cursor movement.
            mPixelFound = true;
        }

        void SendInterrupt()
        {
            // Send a stop signal to the running aimbot thread.
            mChildThread.Stop();
        }

        void MoveCursor(int x, int y)
        {
            // Set the position of the mouse cursor to the effective X and Y positions.
            NativeMethods.SetCursorPos(x, y);

            // Report a SetCursorPos event to the GUI to indicate it's functionality.
            if (Properties.Settings.Default.LogCursorPos)
            {
                AimedAtPosition(new MouseCoordinate(x, y));
            }
        }

        /// <summary>
        /// Stops the aimbot and sets it's state to stopped.
        /// </summary>
        public void StopAimbot()
        {
            // Send interrupt signal to the child thread(s).
            this.SendInterrupt();
            mRunning = false;

            // Only report when user required to.
            if (Properties.Settings.Default.LogStartStop)
            {
                StringBuilder sb = new StringBuilder(DateTime.Now.ToLongTimeString());
                sb.Append(" - ColorAimbot stopped.");
                sb.Append(Environment.NewLine);
                AimbotReports(sb.ToString());
            }
        }

        /// <summary>
        /// Retrieves a list of processes. Used by the GUI to enable the user to select a target process.
        /// </summary>
        /// <returns>A list of processes.</returns>
        public static Process[] GetProcesses()
        {
            return Process.GetProcesses();
        }
    }
}
