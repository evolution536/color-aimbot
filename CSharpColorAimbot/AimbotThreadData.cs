﻿// ===============================================================================================================================
// AimbotBitmapData.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Drawing.Imaging;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Contains the data needed for each worker aimbot thread.
    /// </summary>
    public sealed class AimbotThreadData
    {
        /// <summary>
        /// Byte array containing RGBA byte values of the input bitmap.
        /// </summary>
        public byte[] OriginalBitmapData;

        /// <summary>
        /// The image stride of the input bitmap. The stride is the number of padding bytes until the next bitmap row in memory.
        /// </summary>
        public int Stride;

        /// <summary>
        /// The height of the image in pixels.
        /// </summary>
        public int Height;

        /// <summary>
        /// The width of the image in pixels.
        /// </summary>
        public int Width;

        /// <summary>
        /// The pixel skipping feature pixel count. Pixel skipping is disabled if this value is set to 0.
        /// </summary>
        public int PixelSkipCount;

        /// <summary>
        /// The top position of the target window on the screen. This should be RECT.Top.
        /// </summary>
        public int WindowTopPosition;

        /// <summary>
        /// The left position of the target window on the screen. This should be RECT.Left.
        /// </summary>
        public int WindowLeftPosition;

        /// <summary>
        /// Provides a pointer to the compare function that should be used to match pixels.
        /// </summary>
        public Func<int, int, int, bool> CompareFunction;
    }
}