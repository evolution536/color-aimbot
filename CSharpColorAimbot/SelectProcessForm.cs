﻿// ===============================================================================================================================
// SelectProcessForm.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Enables the user to select a target process. This form can be opened from the MainForm.
    /// </summary>
    public sealed class SelectProcessForm : Form
    {
        MainForm mThis;

        private ListView lvProcesses;
        private Button btnSelect;
        private ColumnHeader colTitle;
        private ColumnHeader colProcessID;
        private Button btnCancel;
    
        public SelectProcessForm(MainForm pThis)
        {
            mThis = pThis;

            InitializeComponent();

            foreach (Process proc in AimbotThreadManager.GetProcesses())
            {
                if (proc.Id > 0)
                {
                    lvProcesses.Items.Add(proc.ProcessName).SubItems.Add(proc.Id.ToString());
                }
            }
        }

        private void InitializeComponent()
        {
            this.lvProcesses = new System.Windows.Forms.ListView();
            this.colTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProcessID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvProcesses
            // 
            this.lvProcesses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTitle,
            this.colProcessID});
            this.lvProcesses.Dock = System.Windows.Forms.DockStyle.Top;
            this.lvProcesses.Location = new System.Drawing.Point(0, 0);
            this.lvProcesses.Name = "lvProcesses";
            this.lvProcesses.Size = new System.Drawing.Size(293, 224);
            this.lvProcesses.TabIndex = 0;
            this.lvProcesses.UseCompatibleStateImageBehavior = false;
            this.lvProcesses.View = System.Windows.Forms.View.Details;
            this.lvProcesses.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvProcesses_KeyUp);
            // 
            // colTitle
            // 
            this.colTitle.Text = "Title";
            this.colTitle.Width = 190;
            // 
            // colProcessID
            // 
            this.colProcessID.Text = "Process ID";
            this.colProcessID.Width = 80;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(12, 230);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 23);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(93, 230);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SelectProcessForm
            // 
            this.AcceptButton = this.btnSelect;
            this.ClientSize = new System.Drawing.Size(293, 261);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.lvProcesses);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectProcessForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Process...";
            this.ResumeLayout(false);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                mThis.mGameProc = lvProcesses.SelectedItems[0].Text;
                mThis.lbSelectedProcess.Text = lvProcesses.SelectedItems[0].Text;
                this.Close();
                return;
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("You must select a process!", "Input Error", MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
            }
        }

        private void lvProcesses_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}