﻿using System;
using System.Windows.Forms;

namespace CSharpColorAimbot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Set the application to always catch unhandled exceptions.
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            // Run the main form.
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            BugReportForm brf = new BugReportForm(e.ExceptionObject as Exception);
            brf.ShowDialog();

            // Force exit of the program to not throw the exception anyway after handling it.
            Environment.Exit(0);
        }
    }
}
