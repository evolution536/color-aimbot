﻿// ===============================================================================================================================
// AimbotConfigurationForm.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Represents the configuration window. The user can configure the aimbot in this window.
    /// </summary>
    public sealed class AimbotConfigurationForm : Form
    {
        private Button btnOK;
        private Button btnCancel;
        private GroupBox gbColorconf;
        private Label lblInfoConfColor;
        private Button btnConfigureColor;

        private GroupBox gbPerfOptions;
        private GroupBox gbPixelSkipping;
        private CheckBox chkEnablePixelSkipping;
        private NumericUpDown numSkippedPixels;
        private Label lblInfoPS;
        private Label lblPSWarning;
        private CheckBox chkLogAimbotTrigger;
        private CheckBox chkLogStartStop;
        private Label lblInfoLogging;
        private ComboBox cbColorSelectionMode;
        private Button btnConfigureColor2;

        Color tmpSelectedColor;
        Color tmpSelectedColor2;

        public AimbotConfigurationForm()
        {
            InitializeComponent();

            tmpSelectedColor = Properties.Settings.Default.Triggercolor;
            tmpSelectedColor2 = Properties.Settings.Default.Triggercolor2;
            this.btnConfigureColor.ForeColor = tmpSelectedColor;
            this.btnConfigureColor2.ForeColor = tmpSelectedColor2;
            this.cbColorSelectionMode.SelectedIndex = Properties.Settings.Default.ColorSelectionMode;

            chkEnablePixelSkipping.Checked = Properties.Settings.Default.EnablePixelSkipping;
            chkEnablePixelSkipping_CheckedChanged(this, EventArgs.Empty);
            numSkippedPixels.Value = Properties.Settings.Default.PixelSkippingNumber > 0 ? Properties.Settings.Default.PixelSkippingNumber : 1;

            chkLogAimbotTrigger.Checked = Properties.Settings.Default.LogCursorPos;
            chkLogStartStop.Checked = Properties.Settings.Default.LogStartStop;
        }

        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbColorconf = new System.Windows.Forms.GroupBox();
            this.btnConfigureColor = new System.Windows.Forms.Button();
            this.lblInfoConfColor = new System.Windows.Forms.Label();
            this.gbPerfOptions = new System.Windows.Forms.GroupBox();
            this.chkLogAimbotTrigger = new System.Windows.Forms.CheckBox();
            this.chkLogStartStop = new System.Windows.Forms.CheckBox();
            this.lblInfoLogging = new System.Windows.Forms.Label();
            this.gbPixelSkipping = new System.Windows.Forms.GroupBox();
            this.lblPSWarning = new System.Windows.Forms.Label();
            this.numSkippedPixels = new System.Windows.Forms.NumericUpDown();
            this.lblInfoPS = new System.Windows.Forms.Label();
            this.chkEnablePixelSkipping = new System.Windows.Forms.CheckBox();
            this.cbColorSelectionMode = new System.Windows.Forms.ComboBox();
            this.btnConfigureColor2 = new System.Windows.Forms.Button();
            this.gbColorconf.SuspendLayout();
            this.gbPerfOptions.SuspendLayout();
            this.gbPixelSkipping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSkippedPixels)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(219, 269);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(300, 269);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbColorconf
            // 
            this.gbColorconf.Controls.Add(this.btnConfigureColor2);
            this.gbColorconf.Controls.Add(this.cbColorSelectionMode);
            this.gbColorconf.Controls.Add(this.btnConfigureColor);
            this.gbColorconf.Controls.Add(this.lblInfoConfColor);
            this.gbColorconf.Location = new System.Drawing.Point(4, 3);
            this.gbColorconf.Name = "gbColorconf";
            this.gbColorconf.Size = new System.Drawing.Size(369, 85);
            this.gbColorconf.TabIndex = 2;
            this.gbColorconf.TabStop = false;
            this.gbColorconf.Text = "Color Configuration";
            // 
            // btnConfigureColor
            // 
            this.btnConfigureColor.Location = new System.Drawing.Point(193, 54);
            this.btnConfigureColor.Name = "btnConfigureColor";
            this.btnConfigureColor.Size = new System.Drawing.Size(82, 23);
            this.btnConfigureColor.TabIndex = 6;
            this.btnConfigureColor.Text = "Configure";
            this.btnConfigureColor.UseVisualStyleBackColor = true;
            this.btnConfigureColor.Click += new System.EventHandler(this.btnConfigureColor_Click);
            // 
            // lblInfoConfColor
            // 
            this.lblInfoConfColor.AutoSize = true;
            this.lblInfoConfColor.Location = new System.Drawing.Point(8, 16);
            this.lblInfoConfColor.Name = "lblInfoConfColor";
            this.lblInfoConfColor.Size = new System.Drawing.Size(319, 26);
            this.lblInfoConfColor.TabIndex = 5;
            this.lblInfoConfColor.Text = "The aimbot\'s trigger color is configurable. Click the button below to\r\nconfigure " +
    "the color to make the aimbot trigger on.";
            // 
            // gbPerfOptions
            // 
            this.gbPerfOptions.Controls.Add(this.chkLogAimbotTrigger);
            this.gbPerfOptions.Controls.Add(this.chkLogStartStop);
            this.gbPerfOptions.Controls.Add(this.lblInfoLogging);
            this.gbPerfOptions.Location = new System.Drawing.Point(4, 173);
            this.gbPerfOptions.Name = "gbPerfOptions";
            this.gbPerfOptions.Size = new System.Drawing.Size(371, 88);
            this.gbPerfOptions.TabIndex = 3;
            this.gbPerfOptions.TabStop = false;
            this.gbPerfOptions.Text = "Logging Options";
            // 
            // chkLogAimbotTrigger
            // 
            this.chkLogAimbotTrigger.AutoSize = true;
            this.chkLogAimbotTrigger.Location = new System.Drawing.Point(11, 55);
            this.chkLogAimbotTrigger.Name = "chkLogAimbotTrigger";
            this.chkLogAimbotTrigger.Size = new System.Drawing.Size(242, 17);
            this.chkLogAimbotTrigger.TabIndex = 2;
            this.chkLogAimbotTrigger.Text = "Log aimbot triggers (SetCursorPos occurance)";
            this.chkLogAimbotTrigger.UseVisualStyleBackColor = true;
            // 
            // chkLogStartStop
            // 
            this.chkLogStartStop.AutoSize = true;
            this.chkLogStartStop.Location = new System.Drawing.Point(11, 32);
            this.chkLogStartStop.Name = "chkLogStartStop";
            this.chkLogStartStop.Size = new System.Drawing.Size(209, 17);
            this.chkLogStartStop.TabIndex = 1;
            this.chkLogStartStop.Text = "Log starting and stopping of the aimbot";
            this.chkLogStartStop.UseVisualStyleBackColor = true;
            // 
            // lblInfoLogging
            // 
            this.lblInfoLogging.AutoSize = true;
            this.lblInfoLogging.Location = new System.Drawing.Point(8, 16);
            this.lblInfoLogging.Name = "lblInfoLogging";
            this.lblInfoLogging.Size = new System.Drawing.Size(270, 13);
            this.lblInfoLogging.TabIndex = 0;
            this.lblInfoLogging.Text = "Logging will be done to the console in the main window.";
            // 
            // gbPixelSkipping
            // 
            this.gbPixelSkipping.Controls.Add(this.lblPSWarning);
            this.gbPixelSkipping.Controls.Add(this.numSkippedPixels);
            this.gbPixelSkipping.Controls.Add(this.lblInfoPS);
            this.gbPixelSkipping.Controls.Add(this.chkEnablePixelSkipping);
            this.gbPixelSkipping.Location = new System.Drawing.Point(4, 94);
            this.gbPixelSkipping.Name = "gbPixelSkipping";
            this.gbPixelSkipping.Size = new System.Drawing.Size(371, 73);
            this.gbPixelSkipping.TabIndex = 4;
            this.gbPixelSkipping.TabStop = false;
            this.gbPixelSkipping.Text = "Pixel Skipping";
            // 
            // lblPSWarning
            // 
            this.lblPSWarning.AutoSize = true;
            this.lblPSWarning.Location = new System.Drawing.Point(206, 16);
            this.lblPSWarning.Name = "lblPSWarning";
            this.lblPSWarning.Size = new System.Drawing.Size(159, 52);
            this.lblPSWarning.TabIndex = 3;
            this.lblPSWarning.Text = "Beware: If the amount of pixels\r\nmatched by the aimbot is lower\r\nthan the amount " +
    "of pixels to skip\r\nthe aimbot will not be triggered!";
            // 
            // numSkippedPixels
            // 
            this.numSkippedPixels.Location = new System.Drawing.Point(80, 42);
            this.numSkippedPixels.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numSkippedPixels.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSkippedPixels.Name = "numSkippedPixels";
            this.numSkippedPixels.Size = new System.Drawing.Size(120, 20);
            this.numSkippedPixels.TabIndex = 2;
            this.numSkippedPixels.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblInfoPS
            // 
            this.lblInfoPS.AutoSize = true;
            this.lblInfoPS.Location = new System.Drawing.Point(3, 44);
            this.lblInfoPS.Name = "lblInfoPS";
            this.lblInfoPS.Size = new System.Drawing.Size(71, 13);
            this.lblInfoPS.TabIndex = 1;
            this.lblInfoPS.Text = "Pixels to skip:";
            // 
            // chkEnablePixelSkipping
            // 
            this.chkEnablePixelSkipping.AutoSize = true;
            this.chkEnablePixelSkipping.Location = new System.Drawing.Point(6, 19);
            this.chkEnablePixelSkipping.Name = "chkEnablePixelSkipping";
            this.chkEnablePixelSkipping.Size = new System.Drawing.Size(125, 17);
            this.chkEnablePixelSkipping.TabIndex = 0;
            this.chkEnablePixelSkipping.Text = "Enable pixel skipping";
            this.chkEnablePixelSkipping.UseVisualStyleBackColor = true;
            this.chkEnablePixelSkipping.CheckedChanged += new System.EventHandler(this.chkEnablePixelSkipping_CheckedChanged);
            // 
            // cbColorSelectionMode
            // 
            this.cbColorSelectionMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbColorSelectionMode.FormattingEnabled = true;
            this.cbColorSelectionMode.Items.AddRange(new object[] {
            "Single Color",
            "Color Range"});
            this.cbColorSelectionMode.Location = new System.Drawing.Point(11, 55);
            this.cbColorSelectionMode.Name = "cbColorSelectionMode";
            this.cbColorSelectionMode.Size = new System.Drawing.Size(176, 21);
            this.cbColorSelectionMode.TabIndex = 8;
            this.cbColorSelectionMode.SelectedIndexChanged += new System.EventHandler(this.cbColorSelectionMode_SelectedIndexChanged);
            // 
            // btnConfigureColor2
            // 
            this.btnConfigureColor2.Location = new System.Drawing.Point(281, 54);
            this.btnConfigureColor2.Name = "btnConfigureColor2";
            this.btnConfigureColor2.Size = new System.Drawing.Size(82, 23);
            this.btnConfigureColor2.TabIndex = 9;
            this.btnConfigureColor2.Text = "Configure";
            this.btnConfigureColor2.UseVisualStyleBackColor = true;
            this.btnConfigureColor2.Click += new System.EventHandler(this.btnConfigureColor2_Click);
            // 
            // AimbotConfigurationForm
            // 
            this.AcceptButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(381, 298);
            this.Controls.Add(this.gbPixelSkipping);
            this.Controls.Add(this.gbPerfOptions);
            this.Controls.Add(this.gbColorconf);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AimbotConfigurationForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure Aimbot";
            this.gbColorconf.ResumeLayout(false);
            this.gbColorconf.PerformLayout();
            this.gbPerfOptions.ResumeLayout(false);
            this.gbPerfOptions.PerformLayout();
            this.gbPixelSkipping.ResumeLayout(false);
            this.gbPixelSkipping.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSkippedPixels)).EndInit();
            this.ResumeLayout(false);

        }

        private void btnConfigureColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = Properties.Settings.Default.Triggercolor;

            if (cd.ShowDialog() == DialogResult.OK)
            {
                tmpSelectedColor = cd.Color;
                Properties.Settings.Default.Triggercolor = tmpSelectedColor;
                this.btnConfigureColor.ForeColor = tmpSelectedColor; 
            }
        }

        private void btnConfigureColor2_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = Properties.Settings.Default.Triggercolor2;

            if (cd.ShowDialog() == DialogResult.OK)
            {
                tmpSelectedColor2 = cd.Color;
                Properties.Settings.Default.Triggercolor2 = tmpSelectedColor2;
                this.btnConfigureColor2.ForeColor = tmpSelectedColor2;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            AimbotThreadManager tmg = AimbotThreadManager.GetInstance();

            Properties.Settings.Default.Triggercolor = tmpSelectedColor;
            Properties.Settings.Default.Triggercolor2 = tmpSelectedColor2;
            tmg.TriggerColor.FirstColor = tmpSelectedColor;
            tmg.TriggerColor.SecondColor = tmpSelectedColor2;
            Properties.Settings.Default.ColorSelectionMode = this.cbColorSelectionMode.SelectedIndex;

            Properties.Settings.Default.PixelSkippingNumber = (int)numSkippedPixels.Value;
            Properties.Settings.Default.EnablePixelSkipping = chkEnablePixelSkipping.Checked;

            Properties.Settings.Default.LogStartStop = chkLogStartStop.Checked;
            Properties.Settings.Default.LogCursorPos = chkLogAimbotTrigger.Checked;

            Properties.Settings.Default.Save();
            this.Close();
        }

        private void chkEnablePixelSkipping_CheckedChanged(object sender, EventArgs e)
        {
            lblInfoPS.Enabled = chkEnablePixelSkipping.Checked;
            lblPSWarning.Enabled = chkEnablePixelSkipping.Checked;
            numSkippedPixels.Enabled = chkEnablePixelSkipping.Checked;
        }

        private void cbColorSelectionMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.cbColorSelectionMode.SelectedIndex)
            {
                case 0:
                    this.btnConfigureColor2.Visible = false;
                    break;
                case 1:
                    this.btnConfigureColor2.Visible = true;
                    break;
            }
        }
    }
}