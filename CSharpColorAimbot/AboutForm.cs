﻿// ===============================================================================================================================
// AboutForm.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Represents the about dialog. Contains information about the program, author and website.
    /// </summary>
    public sealed class AboutForm : Form
    {
        private PictureBox pbAboutImage;
        private LinkLabel lnkUCForum;
        private Label lblAboutText;
        private Button btnClose;
    
        public AboutForm()
        {
            InitializeComponent();

            lnkUCForum.Links.Add(0, lnkUCForum.Text.Length, "http://www.unknowncheats.me/forum/c/86770-performant-color-aimbot.html");
        }

        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.pbAboutImage = new System.Windows.Forms.PictureBox();
            this.lnkUCForum = new System.Windows.Forms.LinkLabel();
            this.lblAboutText = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbAboutImage)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(197, 106);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pbAboutImage
            // 
            this.pbAboutImage.Image = global::CSharpColorAimbot.Properties.Resources.AboutImage;
            this.pbAboutImage.Location = new System.Drawing.Point(1, 1);
            this.pbAboutImage.Name = "pbAboutImage";
            this.pbAboutImage.Size = new System.Drawing.Size(128, 128);
            this.pbAboutImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbAboutImage.TabIndex = 1;
            this.pbAboutImage.TabStop = false;
            // 
            // lnkUCForum
            // 
            this.lnkUCForum.AutoSize = true;
            this.lnkUCForum.Location = new System.Drawing.Point(135, 62);
            this.lnkUCForum.Name = "lnkUCForum";
            this.lnkUCForum.Size = new System.Drawing.Size(94, 13);
            this.lnkUCForum.TabIndex = 3;
            this.lnkUCForum.TabStop = true;
            this.lnkUCForum.Text = "UnKnoWnCheaTs";
            this.lnkUCForum.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkUCForum_LinkClicked);
            // 
            // lblAboutText
            // 
            this.lblAboutText.AutoSize = true;
            this.lblAboutText.Location = new System.Drawing.Point(135, 9);
            this.lblAboutText.Name = "lblAboutText";
            this.lblAboutText.Size = new System.Drawing.Size(83, 39);
            this.lblAboutText.TabIndex = 2;
            this.lblAboutText.Text = "C# Color Aimbot\r\nby evolution536\r\nVersion 1.03";
            // 
            // AboutForm
            // 
            this.AcceptButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(278, 133);
            this.Controls.Add(this.lnkUCForum);
            this.Controls.Add(this.lblAboutText);
            this.Controls.Add(this.pbAboutImage);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About...";
            ((System.ComponentModel.ISupportInitialize)(this.pbAboutImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lnkUCForum_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
        }
    }
}