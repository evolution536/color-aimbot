﻿// ===============================================================================================================================
// MouseCoordinate.cs, part of CSharpColorAimbot
// Author: evolution536 - UnKnoWnCheaTs
// ===============================================================================================================================

using System;

namespace CSharpColorAimbot
{
    /// <summary>
    /// Contains a set of coordinates on screen. Used by the aimbot to move the mouse and report to the logging system.
    /// </summary>
    public sealed class MouseCoordinate
    {
        /// <summary>
        /// The X position on the screen of the mouse.
        /// </summary>
        public int X;

        /// <summary>
        /// The Y position on the screen of the mouse.
        /// </summary>
        public int Y;

        public MouseCoordinate(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}