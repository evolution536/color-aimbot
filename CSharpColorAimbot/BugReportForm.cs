﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace CSharpColorAimbot
{
    public sealed class BugReportForm : Form
    {
        private TextBox tbExceptionDetails;
        private Button btnCopyExceptionData;
        private Button btnReportBug;
        private Button btnClose;
        private Label lblMainInfo;
    
        public BugReportForm(Exception pException)
        {
            InitializeComponent();

            tbExceptionDetails.Text = "Exception message:" + Environment.NewLine + Environment.NewLine;
            tbExceptionDetails.Text += pException.Message + Environment.NewLine + Environment.NewLine;
            tbExceptionDetails.Text += "Thrown by:" + Environment.NewLine + Environment.NewLine;
            tbExceptionDetails.Text += pException.Source + Environment.NewLine + Environment.NewLine;
            tbExceptionDetails.Text += "Stack trace:" + Environment.NewLine + Environment.NewLine;
            tbExceptionDetails.Text += pException.StackTrace;

            
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BugReportForm));
            this.lblMainInfo = new System.Windows.Forms.Label();
            this.tbExceptionDetails = new System.Windows.Forms.TextBox();
            this.btnCopyExceptionData = new System.Windows.Forms.Button();
            this.btnReportBug = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMainInfo
            // 
            this.lblMainInfo.AutoSize = true;
            this.lblMainInfo.Location = new System.Drawing.Point(12, 9);
            this.lblMainInfo.Name = "lblMainInfo";
            this.lblMainInfo.Size = new System.Drawing.Size(475, 52);
            this.lblMainInfo.TabIndex = 0;
            this.lblMainInfo.Text = resources.GetString("lblMainInfo.Text");
            // 
            // tbExceptionDetails
            // 
            this.tbExceptionDetails.Location = new System.Drawing.Point(15, 93);
            this.tbExceptionDetails.Multiline = true;
            this.tbExceptionDetails.Name = "tbExceptionDetails";
            this.tbExceptionDetails.ReadOnly = true;
            this.tbExceptionDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbExceptionDetails.Size = new System.Drawing.Size(488, 218);
            this.tbExceptionDetails.TabIndex = 1;
            this.tbExceptionDetails.TabStop = false;
            // 
            // btnCopyExceptionData
            // 
            this.btnCopyExceptionData.Location = new System.Drawing.Point(15, 64);
            this.btnCopyExceptionData.Name = "btnCopyExceptionData";
            this.btnCopyExceptionData.Size = new System.Drawing.Size(208, 23);
            this.btnCopyExceptionData.TabIndex = 2;
            this.btnCopyExceptionData.Text = "Copy exception data to clipboard";
            this.btnCopyExceptionData.UseVisualStyleBackColor = true;
            this.btnCopyExceptionData.Click += new System.EventHandler(this.btnCopyExceptionData_Click);
            // 
            // btnReportBug
            // 
            this.btnReportBug.Location = new System.Drawing.Point(229, 64);
            this.btnReportBug.Name = "btnReportBug";
            this.btnReportBug.Size = new System.Drawing.Size(139, 23);
            this.btnReportBug.TabIndex = 3;
            this.btnReportBug.Text = "Report Bug";
            this.btnReportBug.UseVisualStyleBackColor = true;
            this.btnReportBug.Click += new System.EventHandler(this.btnReportBug_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(374, 64);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(129, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // BugReportForm
            // 
            this.ClientSize = new System.Drawing.Size(515, 323);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReportBug);
            this.Controls.Add(this.btnCopyExceptionData);
            this.Controls.Add(this.tbExceptionDetails);
            this.Controls.Add(this.lblMainInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BugReportForm";
            this.ShowIcon = false;
            this.Text = "Report Bug";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCopyExceptionData_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(tbExceptionDetails.Text, TextDataFormat.Text);
            MessageBox.Show("The exception details are copied to the clipboard.", "Operation Completed"
                , MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnReportBug_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.unknowncheats.me/forum/c/86770-performant-color-aimbot.html");
        }
    }
}